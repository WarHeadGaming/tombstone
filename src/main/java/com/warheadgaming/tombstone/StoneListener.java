/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.com.warheadgaming.tombstone;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.spout.api.entity.Player;
import org.spout.api.event.EventHandler;
import org.spout.api.event.Listener;
import org.spout.api.event.Order;
import org.spout.api.event.cause.PluginCause;
import org.spout.api.geo.World;
import org.spout.api.geo.cuboid.Block;
import org.spout.api.material.block.BlockFace;
import org.spout.vanilla.api.event.player.PlayerDeathEvent;
import org.spout.vanilla.plugin.component.inventory.PlayerInventory;
import org.spout.vanilla.plugin.component.substance.material.Sign;
import org.spout.vanilla.plugin.component.substance.material.chest.Chest;
import org.spout.vanilla.plugin.material.VanillaMaterials;
import org.spout.vanilla.plugin.material.block.misc.Slab;

/**
 *
 * @author DarkCloud
 */
public class StoneListener implements Listener {

    private Tombstone plugin;
    DateFormat df = new SimpleDateFormat("MM/dd/yy hh:mm");
    Date today = Calendar.getInstance().getTime();
    String Date = df.format(today);
    public String playerName;
    

    public void StoneListner() {
        plugin = Tombstone.getInstance();
    }

    @EventHandler(order = Order.EARLIEST)
    public void DeathEvent(PlayerDeathEvent event) {
        
        Player player = event.getPlayer();
        createTombstone(event.getPlayer());
        createCasket(event.getPlayer());
    }

    public void createTombstone(Player player) {
        
        Block block = null;
        
        //Get the world and player location
        World world = player.getWorld();
        float x = player.getScene().getPosition().getX();
        float y = player.getScene().getPosition().getY();
        float z = player.getScene().getPosition().getZ();
        
        //create the headstone and structures around your place of death
        block = player.getWorld().getBlock(x, y - 1, z);
        block.setMaterial(VanillaMaterials.STONE);

        block = player.getWorld().getBlock(x, y, z);
        block.setMaterial(VanillaMaterials.STONE);

        block = world.getBlock(x, y + 1, z);
        block.setMaterial(Slab.STONE_BRICK);

        block = player.getWorld().getBlock(x, y, z - 1);
        block.setMaterial(Slab.STONE_BRICK);

        block = player.getWorld().getBlock(x + 1, y, z);
        block.setMaterial(VanillaMaterials.STAIRS_STONE_BRICK);

        block = player.getWorld().getBlock(x - 1, y, z);
        block.setMaterial(VanillaMaterials.STAIRS_STONE_BRICK);
        
        //Place some flowers to mourn your death =(
        block = player.getWorld().getBlock(x + 1, y, z + 1);
        block.setMaterial(VanillaMaterials.FLOWER_POT_BLOCK.ROSE);

        block = player.getWorld().getBlock(x - 1, y, z + 1);
        block.setMaterial(VanillaMaterials.FLOWER_POT_BLOCK.ROSE);
        
        //Get the headstone face and place a sign on it
        block = player.getWorld().getBlock(x, y, z + 1);
        VanillaMaterials.WALL_SIGN.setAttachedFace(block, BlockFace.EAST, new PluginCause(plugin));
        ((Sign) block.getComponent()).setText(new String[]{"Here Lies", playerName, "Died", Date}, new PluginCause(plugin));
    }

    public void createCasket(Player player) {
        
        Block block = null;
        
        //get the player location and world
        World world = player.getWorld();
        float x = player.getScene().getPosition().getX();
        float y = player.getScene().getPosition().getY();
        float z = player.getScene().getPosition().getZ();
        block = player.getWorld().getBlock(x, y - 2, z + 1);
        //Create a double chest!
        VanillaMaterials.CHEST.isDouble(block);

        //Add your stuffs into the chest.
        Chest chest = (Chest) block.getComponent();
        PlayerInventory inv = player.get(PlayerInventory.class);

        if (inv != null) {
            chest.getInventory().addAll(inv.getMain());
            chest.getInventory().addAll(inv.getArmor());
        }
    }
}
