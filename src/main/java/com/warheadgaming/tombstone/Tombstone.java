/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main.java.com.warheadgaming.tombstone;

import org.spout.api.plugin.CommonPlugin;

/**
 *
 * @author DarkCloud
 */
public class Tombstone extends CommonPlugin {

    private static Tombstone plugin;
    
    
    @Override
    public void onEnable() {
        plugin = this;
        
        getLogger().info("Enabled!");
        getEngine().getEventManager().registerEvents(new StoneListener(), this);
    }

    @Override
    public void onDisable() {
        getLogger().info("Disabled!");
    }
    
        public static Tombstone getInstance() {
        return plugin;

    }
    
}
